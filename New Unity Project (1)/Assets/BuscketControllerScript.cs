﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuscketControllerScript : MonoBehaviour
{
    GameObject Arrow;
    GameObject BallPrefab;
    float px = 0;
  
    void Start()
    {
        this.BallPrefab = GameObject.Find("BallPrefab");
        px = Random.Range(0.1f, 0.2f);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(px, 0, 0);

        if (transform.position.x > 10.0f)
        {
            Destroy(gameObject);
        }

    }
}
