﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    Rigidbody2D rigit2D;
    float BoundForse = 280.0f;


    // Start is called before the first frame update
    void Start()
    {
        this.rigit2D = GetComponent<Rigidbody2D>();
        this.rigit2D.AddForce(transform.up * this.BoundForse);
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y < -10.0f)
        {
            Destroy(gameObject);
        }
    }
     void OnTriggerEnter2D(Collider2D other)
    {

        Debug.Log("ゴール");
        Destroy(gameObject);
      
        GameObject Director = GameObject.Find("GameDirector");
        Director.GetComponent<GameDirector>().DecreaseCo();
    }
}