﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class GameDirector : MonoBehaviour
{

    //他のスクリプトに移せる変数

    public static int Co = 0;
    public static int Co2 = 0;
    public static int Co3 = 0;
    public static float time = 1800.0f;
    public GameObject BallCount = null;  //0を意味する
    GameObject timer;

    // Start is called before the first frame update
    void Start()
    {
        this.timer = GameObject.Find("timer"); //探す
        time = 30.0f; //初期時間
        Co = 0;
        Co2 = 0;
        Co3 = 0;
    }

    public void DecreaseCo()
    {
        Co += 2;
    }
    public void DecreaseCo()
    {
        Co2 += 5;
    }
    public void DecreaseCo()
    {
        Co3 -= 6;
    }
    // Update is called once per frame
    void Update()
    {
        Text arrowText = this.BallCount.GetComponent<Text>();
        arrowText.text = Co + "個";  //スコア表示

        time -= Time.deltaTime;
        this.timer.GetComponent<Image>().fillAmount -= 0.00056f; //タイムリミット
    }
}
